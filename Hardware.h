#ifndef HARDWARE_H
#define HARDWARE_H

/*-------------------------------------------------------------*/
/*      Configure the harware platform                          */
/*-------------------------------------------------------------*/
// User inteface buttons
#define CONFIG_UPBTN_PIN      2
#define CONFIG_STARTBTN_PIN   3
#define CONFIG_DOWNBTN_PIN    4
// Solid state relay
#define CONFIG_SSR_PIN        5
// Thenmocouple module pins
#define CONFIG_TCGND_PIN      8
#define CONFIG_TCVCC_PIN      9
#define CONFIG_TCSCK_PIN      10
#define CONFIG_TCCS_PIN       11
#define CONFIG_TCDO_PIN       12

#endif

