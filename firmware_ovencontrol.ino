#include "LiquidCrystal_I2C.h"
#include "GFEEPROM.h"
#include "GFButton.h"
#include "max6675.h"
#include "GFLed.h"
#include "Hardware.h"

// Objects required for this project
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
MAX6675 thermocouple(CONFIG_TCSCK_PIN, CONFIG_TCCS_PIN, CONFIG_TCDO_PIN);
GFButton startButton(CONFIG_STARTBTN_PIN);
GFButton upButton(CONFIG_UPBTN_PIN);
GFButton downButton(CONFIG_DOWNBTN_PIN);
GFLed statusLed;

// Global variables
uint16_t ovenSetpoint = 0;
uint32_t lastTime = 0;
bool needsRedraw = true;
double temperature;
enum OvenStates {E_OVENCTRL_ADJUST, E_OVENCTRL_HEAT} state;

void setup() {
  // Initialize pins
  pinMode(CONFIG_SSR_PIN, OUTPUT); digitalWrite(CONFIG_SSR_PIN, LOW);
#if defined(CONFIG_TCVCC_PIN) && defined(CONFIG_TCGND_PIN)
  pinMode(CONFIG_TCVCC_PIN, OUTPUT); digitalWrite(CONFIG_TCVCC_PIN, HIGH);
  pinMode(CONFIG_TCGND_PIN, OUTPUT); digitalWrite(CONFIG_TCGND_PIN, LOW);
#endif

  // Read setting from EEPROM and set appropiate default
  ovenSetpoint = EEPROM.readWord(0x0000);
  if (ovenSetpoint < 150 || ovenSetpoint > 300) {
    EEPROM.updateWord(0x0000, 230);
  }

  // Prepare the serial interface
  Serial.begin(9600);

  // Preoare the LCD
  lcd.begin(16, 2);

  // Show dialog to serial monitor
  Serial.println(F("----------------------------------------------------"));
  Serial.println(F("            BASIC REFLOW OVEN CONTROLLER            "));
  Serial.println(F("             https://www.geekfactory.mx             "));
  Serial.println(F("----------------------------------------------------"));
}

void loop() {

  switch (state)
  {
    case E_OVENCTRL_ADJUST:
      // Keep SSR pin low
      digitalWrite(CONFIG_SSR_PIN, LOW);
      // Process buttons
      if (upButton.wasPressed()) {
        ovenSetpoint++;
        ovenSetpoint = constrain(ovenSetpoint, 150, 300);
        EEPROM.updateWord(0x0000, ovenSetpoint);
        needsRedraw = true;
      } else if (downButton.wasPressed()) {
        ovenSetpoint--;
        ovenSetpoint = constrain(ovenSetpoint, 150, 300);
        EEPROM.updateWord(0x0000, ovenSetpoint);
        needsRedraw = true;
      } else if (startButton.wasPressed()) {
        state = E_OVENCTRL_HEAT;
        lastTime = millis();
        lcd.clear();
      }
      // Redraw the screen if something changed
      if (needsRedraw) {
        lcd.clear();
        lcd.print(" Configure Temp ");
        lcd.setCursor(6, 1);
        lcd.print(ovenSetpoint);
        needsRedraw = false;
      }
      break;

    case E_OVENCTRL_HEAT:
      // Keep SSR pin high
      digitalWrite(CONFIG_SSR_PIN, HIGH);
      // Measure the temperature every second
      if (startButton.wasPressed()) {
        state = E_OVENCTRL_ADJUST;
        needsRedraw = true;
      }
      if (millis() - lastTime >= 1000) {
        lastTime = millis();
        // Read thermocouple
        temperature = thermocouple.readCelsius();
        // Print current temperature
        lcd.clear();
        lcd.print("   Heating...");
        lcd.setCursor(6, 1);
        lcd.print(temperature);
        // Check if the oven reached the configured temperature
        if (temperature >= ovenSetpoint) {
          state = E_OVENCTRL_ADJUST;
          needsRedraw = true;
        }
      }
      break;
  }

  // Process led blinking
  statusLed.process();
}
